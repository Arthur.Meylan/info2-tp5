import os
import re

import nltk as nltk

vocabulary = {}


def list_filepaths(folder):
    # consultez la documentation des modules os et os.path pour vous aider
    return [os.path.join(folder, file) for file in os.listdir(folder)]

def load_text(filepath):
    raw = open(filepath, 'r', encoding='utf8').read()
    return re.sub(r'^[\S\s]+[*]{3} START OF .+[*]{3}([\S\s]+)[*]{3} END OF .*[*]{3}[\S\s]+$', r'\1', raw)


def preprocess(text):
    # nltk.download('punkt')
    # nltk.download('stopwords')
    stopwords = set(nltk.corpus.stopwords.words('french'))
    stemmer = nltk.stem.snowball.FrenchStemmer()
    tokens = []
    for line in text.strip().split('\n\n'):
        if not re.match(r'\s+', line):
            tokens.extend([stemmer.stem(t) for t in nltk.word_tokenize(line.strip().lower(), "french")
                           if t.isalpha() and t not in stopwords])
    return tokens


def extract_ngrams(tokens, n=1):
    ngrams = {}
    for i in range(len(tokens)-n+1):
        ngram = ''
        #Construit une string avec n mots dedans
        for j in range(n):
            ngram += tokens[i + j]
            if j != n-1:
                ngram += ' '


        if ngram not in ngrams:
            ngrams[ngram] = 1
        else:
            ngrams[ngram] = ngrams.get(ngram) + 1

    return ngrams


def fill_vocabulary(ngrams):

    for ngram in ngrams.keys():
        if ngram not in vocabulary:
            vocabulary[ngram] = ngrams.get(ngram)
        else:
            vocabulary[ngram] = vocabulary.get(ngram) + ngrams.get(ngram)


def plot_frequency_distribution(tokens):
    fdist = nltk.probability.FreqDist(tokens)
    fdist.plot(50, cumulative=True)


if __name__ == '__main__':

    for filepath in list_filepaths('data'):
        tokens = preprocess(load_text(filepath))
        ngrams = extract_ngrams(tokens, 2)
        fill_vocabulary(ngrams)
    plot_frequency_distribution(vocabulary)
